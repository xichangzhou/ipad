//
//  LoginViewController.swift
//  Ipad
//
//  Created by Li Song on 16/3/24.
//  Copyright © 2016年 Li Song. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController ,UITextFieldDelegate{
  @IBOutlet weak var username: UITextField!

  @IBOutlet weak var password: UITextField!
  
  var isHeightChanged = false
  
  func initDelegate() {
    username.delegate = self
    password.delegate = self
  }
  
  func initKeyboardDelegate() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillAppear(_:)), name: UIKeyboardWillShowNotification, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillDisappear(_:)), name:UIKeyboardWillHideNotification, object: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1)
    initKeyboardDelegate()
    initDelegate()
  }
  
  @IBAction func loginBtn(sender: UIButton) {
  }
  
  //当键盘升起来的时候提高位置，根据键盘升起来多少位置提高多少
  func keyboardWillAppear(notification: NSNotification) {
    
    // 获取键盘信息
    let keyboardinfo = notification.userInfo![UIKeyboardFrameBeginUserInfoKey]
    
    let keyboardheight:CGFloat = (keyboardinfo?.CGRectValue.size.height)!

    if(UIScreen.mainScreen().bounds.height-self.password.frame.origin.y < keyboardheight) {
      isHeightChanged = true
      UIView.animateWithDuration(0.2, animations: {
        self.view.frame.origin.y = -keyboardheight
      })
    }
    
    
  }
  
  //在键盘收起来的时候恢复位置
  func keyboardWillDisappear(notification:NSNotification){
    
    if isHeightChanged {
      UIView.animateWithDuration(0.1, animations: {
        self.view.frame.origin.y = 0
      })
    }
    
  }
  
  //点击键盘上的return键隐藏键盘
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  //点击屏幕其他空白地方隐藏键盘
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    username.resignFirstResponder()
    password.resignFirstResponder()
  }

}
