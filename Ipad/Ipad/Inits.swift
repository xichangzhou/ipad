//
//  Init.swift
//  Ipad
//
//  Created by admin on 16/3/28.
//  Copyright © 2016年 Li Song. All rights reserved.
//

import Foundation
import UIKit

class Inits {
    //这个是用在DateCecordViewController里面初始化右滑的视图
    class func initLeftTableView(view : UIView,delegate: UITableViewDelegate,dataSource:UITableViewDataSource) {
        let tableview = UITableView(frame: CGRectMake(0,40,view.frame.size.width + 16,view.frame.size.height))
        tableview.numberOfRowsInSection(1)
        tableview.estimatedRowHeight = 95
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.dataSource = dataSource
        tableview.delegate = delegate
        tableview.tag = 1000
        let nib = UINib(nibName: "CustomerLeftTableViewCell", bundle: nil)
        tableview.registerNib(nib, forCellReuseIdentifier: "customerLeftCell")
        
        let settingView = UIImageView(image: UIImage(named: "ios-settings-strong - Ionicons"))
        settingView.frame = CGRectMake(20, 10, 15, 15)
        let showDetailView = UIImageView(image: UIImage(named: "expand - FontAwesome"))
        showDetailView.frame = CGRectMake(view.frame.width-20, 10, 15, 15)
        
        let goalsLabel = UILabel(frame: CGRectMake(view.frame.width/2-100,10,200,20))
        goalsLabel.text = "GOALS"
        goalsLabel.textAlignment = NSTextAlignment.Center
        goalsLabel.textColor = .redColor()
        goalsLabel.font = UIFont.boldSystemFontOfSize(20)
        view.addSubview(goalsLabel)
        view.addSubview(showDetailView)
        view.addSubview(settingView)
        view.addSubview(tableview)
    }
    
    //这个是用在DateCecordViewController里面初始化左滑的视图
    class func initToolView(dicViews : [String : UIView]) {
        dicViews["homeView"]!.addSubview(dicViews["toolView"]!)
        dicViews["toolView"]!.frame.size.width = 100
        dicViews["toolView"]!.backgroundColor = UIColor.grayColor()
        
        (dicViews["timerImageView"]! as! UIImageView).image = UIImage(named: "Timer")
        dicViews["timerImageView"]!.userInteractionEnabled = true
        dicViews["toolView"]!.addSubview(dicViews["timerImageView"]!)
        
        (dicViews["durationImageView"]! as! UIImageView).image = UIImage(named: "Duration")
        dicViews["durationImageView"]!.userInteractionEnabled = true
        dicViews["toolView"]!.addSubview(dicViews["durationImageView"]!)
        
        let frequencyImage = UIImage(named: "Frequency")
        (dicViews["frequencyImageView"]! as! UIImageView).image = frequencyImage
        dicViews["frequencyImageView"]!.userInteractionEnabled = true
        dicViews["toolView"]!.addSubview(dicViews["frequencyImageView"]!)
        
        let historyImage = UIImage(named: "History")
        (dicViews["historyImageView"]! as! UIImageView).image = historyImage
        dicViews["historyImageView"]!.userInteractionEnabled = true
        dicViews["toolView"]!.addSubview(dicViews["historyImageView"]!)
        
        let linView = UIView(frame: CGRectMake(0, 340, 100, 1))
        linView.backgroundColor = UIColor.whiteColor()
        dicViews["toolView"]!.addSubview(linView)
        
        let image04 = UIImage(named: "Duration-1")
        (dicViews["imageView04"]! as! UIImageView).image = image04
        dicViews["toolView"]!.addSubview(dicViews["imageView04"]!)
        dicViews["imageView04"]!.userInteractionEnabled = true
        
        let imageView05 = UIImageView(frame: CGRectMake(20, 470, 40, 75))
        let image05 = UIImage(named: "Duration-2")
        imageView05.image = image05
        dicViews["toolView"]!.addSubview(imageView05)
        
        let imageView06 = UIImageView(frame: CGRectMake(20, 565, 40, 75))
        let image06 = UIImage(named: "Frequency-1")
        imageView06.image = image06
        dicViews["toolView"]!.addSubview(imageView06)
    }
    
    
    class func initLabel(label : UILabel) {
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    class func initBtn(btn : UIButton) {
        btn.setTitleColor(UIColor.init(red: 26 / 255, green: 174 / 255, blue: 253 / 255, alpha: 1), forState: UIControlState.Normal)
        btn.layer.cornerRadius = 6
        btn.layer.borderColor = UIColor.init(red: 26 / 255, green: 174 / 255, blue: 253 / 255, alpha: 1).CGColor
        btn.layer.borderWidth = 1
    }
    
    class func initBtn(rect: CGRect , named : String) -> UIButton{
        let btn = UIButton(frame: rect)
        btn.setTitleColor(UIColor.init(red: 26 / 255, green: 174 / 255, blue: 253 / 255, alpha: 1), forState: UIControlState.Normal)
        btn.layer.cornerRadius = 6
        btn.layer.borderColor = UIColor.init(red: 26 / 255, green: 174 / 255, blue: 253 / 255, alpha: 1).CGColor
        btn.layer.borderWidth = 1
        return btn
    }
    
    class func initImageView(rect: CGRect , named : String) -> UIImageView{
        let imageView = UIImageView(frame: rect)
        imageView.image = UIImage(named: named)
        return imageView
    }
    
    class func initLabel(rect: CGRect , named : String , fontSize : CGFloat , textAlignment : NSTextAlignment) -> UILabel {
        let label = UILabel(frame: rect)
        label.text = named
        label.font = UIFont.systemFontOfSize(fontSize)
        label.textAlignment = textAlignment
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor.blackColor().CGColor
        return label
    }
    
    class func initTextField(rect: CGRect , named : String , fontSize : CGFloat , textAlignment : NSTextAlignment , textColor : UIColor , delegate: UITextFieldDelegate) -> UITextField {
        let textField = UITextField(frame: rect)
        textField.text = named
        textField.font = UIFont.systemFontOfSize(fontSize)
        textField.textAlignment = textAlignment
        textField.textColor = textColor
        textField.delegate = delegate
        return textField
    }
    
}