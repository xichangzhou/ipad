//
//  ViewController.swift
//  Ipad01
//
//  Created by admin on 16/3/22.
//  Copyright © 2016年 admin. All rights reserved.
//

import UIKit

class DateCecordViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tapGeneratlizationView: UIView!
    @IBOutlet weak var tapBiView: UIView!
    @IBOutlet weak var biView: UIView!
    @IBOutlet weak var generatlizationView: UIView!
    @IBOutlet var homeView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var rightView: UIView!
    
    @IBOutlet weak var independentBtn: UIButton!
    @IBOutlet weak var fvmBtn: UIButton!
    @IBOutlet weak var pvpBtn: UIButton!
    
    @IBOutlet weak var trialMin: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minLabel: UILabel!
    
    
    
    @IBOutlet weak var customerTableView: UITableView!
    
    @IBOutlet weak var notes: UITextField!
    
    @IBOutlet weak var generatlizationNotes: UITextField!
    
    @IBOutlet weak var generatilizationMin: UILabel!
    
    @IBOutlet weak var generatilizationTrial: UILabel!
    
    @IBOutlet weak var generatilizationMonBtn: UIButton!
    
    @IBOutlet weak var generatilizationDadBtn: UIButton!
    
    @IBOutlet weak var generatilizationSiblingBtn: UIButton!
    
    @IBOutlet weak var generatilizationOtherBtn: UIButton!
    
    @IBOutlet weak var generatilizationIndependentBtn: UIButton!
    
    @IBOutlet weak var generatilizationPvpBtn: UIButton!
    
    @IBOutlet weak var generatilizationFvmBtn: UIButton!
    
    @IBOutlet weak var generatilizationTimeLabel: UILabel!
    
    @IBOutlet weak var generatilizationUpBtn: UIButton!
    
    @IBOutlet weak var generatilizationDownBtn: UIButton!
    
    @IBOutlet weak var generatilizationAnother: UIButton!
    var generatilizationAddAnotherTextField = false
    
    @IBOutlet weak var generatiliztionAnotherToTop: NSLayoutConstraint!
    
    
    @IBAction func independentBtn(sender: UIButton) {
        cleanAllBtnColor()
        independentBtn.backgroundColor = UIColor.lightGrayColor()
    }
    @IBAction func fvmBtn(sender: UIButton) {
        cleanAllBtnColor()
        fvmBtn.backgroundColor = UIColor.lightGrayColor()
    }
    @IBAction func pvpBtn(sender: UIButton) {
        cleanAllBtnColor()
        pvpBtn.backgroundColor = UIColor.lightGrayColor()
    }
    @IBAction func minusBtn(sender: UIButton) {
        trialMin.text = "\(Int(trialMin.text!)! - 1)"
        let s = (minLabel.text! as NSString).substringToIndex(7)
        let ss = (minLabel.text! as NSString).substringFromIndex(7)
        let sss = Int(ss)! - 1
        minLabel.text = s + "\(sss)"
        cleanAllBtnColor()
        minusBtn.backgroundColor = UIColor.lightGrayColor()
    }
    @IBAction func plusBtn(sender: UIButton) {
        trialMin.text = "\(Int(trialMin.text!)! + 1)"
        let s = (minLabel.text! as NSString).substringToIndex(7)
        let ss = (minLabel.text! as NSString).substringFromIndex(7)
        let sss = Int(ss)! + 1
        minLabel.text = s + "\(sss)"
        cleanAllBtnColor()
        plusBtn.backgroundColor = UIColor.lightGrayColor()
    }
    @IBAction func generatilizationBtn(sender: UIButton) {
        if sender.tag == 200 {
            cleanAllBtnColor()
            generatilizationMonBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 201 {
            cleanAllBtnColor()
            generatilizationDadBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 202 {
            cleanAllBtnColor()
            generatilizationSiblingBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 203 {
            cleanAllBtnColor()
            generatilizationOtherBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 204 {
            cleanAllBtnColor()
            generatilizationIndependentBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 205 {
            cleanAllBtnColor()
            generatilizationPvpBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 206 {
            cleanAllBtnColor()
            generatilizationFvmBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 207 {
            generatilizationTrial.text = "\(Int(generatilizationTrial.text!)! + 1)"
            let s = (generatilizationMin.text! as NSString).substringToIndex(7)
            let ss = (generatilizationMin.text! as NSString).substringFromIndex(7)
            let sss = Int(ss)! + 1
            generatilizationMin.text = s + "\(sss)"
            cleanAllBtnColor()
            generatilizationUpBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 208 {
            generatilizationTrial.text = "\(Int(generatilizationTrial.text!)! - 1)"
            let s = (generatilizationMin.text! as NSString).substringToIndex(7)
            let ss = (generatilizationMin.text! as NSString).substringFromIndex(7)
            let sss = Int(ss)! - 1
            generatilizationMin.text = s + "\(sss)"
            cleanAllBtnColor()
            generatilizationDownBtn.backgroundColor = UIColor.lightGrayColor()
        } else if sender.tag == 209 {
            if !generatilizationAddAnotherTextField {
                let y = generatlizationNotes.frame.origin.y
                let textField = Inits.initTextField(CGRectMake(20,y + 40,UIScreen.mainScreen().bounds.width * 7/12  * 0.9,30), named: "", fontSize: 17, textAlignment: NSTextAlignment.Left, textColor: UIColor.redColor(), delegate: self)
                textField.layer.cornerRadius = 5
                textField.backgroundColor = UIColor.lightGrayColor()
                generatlizationView.addSubview(textField)
                //这里是动态的修改约束
                generatiliztionAnotherToTop.constant = y + 40 * 2
                generatilizationAddAnotherTextField = true
            }
        }
    }
    
    func cleanAllBtnColor() {
        independentBtn.backgroundColor = UIColor.clearColor()
        fvmBtn.backgroundColor = UIColor.clearColor()
        pvpBtn.backgroundColor = UIColor.clearColor()
        plusBtn.backgroundColor = UIColor.clearColor()
        minusBtn.backgroundColor = UIColor.clearColor()
        
        generatilizationMonBtn.backgroundColor = UIColor.clearColor()
        generatilizationDadBtn.backgroundColor = UIColor.clearColor()
        generatilizationSiblingBtn.backgroundColor = UIColor.clearColor()
        generatilizationOtherBtn.backgroundColor = UIColor.clearColor()
        generatilizationIndependentBtn.backgroundColor = UIColor.clearColor()
        generatilizationPvpBtn.backgroundColor = UIColor.clearColor()
        generatilizationFvmBtn.backgroundColor = UIColor.clearColor()
        generatilizationUpBtn.backgroundColor = UIColor.clearColor()
        generatilizationDownBtn.backgroundColor = UIColor.clearColor()
    }
    
    var names = ["Cookie","Swing","Legos","Toy Car","Juice","Chips"]
    
    //这个是控制左滑和右滑的变量
    var isLeftTableViewShow = false
    var isToolViewShow = false
    
    //左右视图滑动手势
    let swipeRight = UISwipeGestureRecognizer()
    let swipeLeft = UISwipeGestureRecognizer()
    
    //这个是点击right视图上面的内容的手势
    let tapGeneratlizationViewGesture = UITapGestureRecognizer()
    let tapBiViewGesture = UITapGestureRecognizer()
    
    //这个是点击home视图的手势
    let tapHomeViewGesture = UITapGestureRecognizer()
    
    
    //这个是左滑出现的工具视图
    let toolView = UIView(frame: CGRectMake(UIScreen.mainScreen().bounds.width - 100, 64, 0, UIScreen.mainScreen().bounds.height - 64))
    
    //这个是点击工具视图然后出现的视图内容
    let popView  = UIView()
    //popupView是包含在popView中的
    let popupView  = UIView()
    
    //这个是右边工具栏上点击相应按钮出现的视图上右上角的delete按钮
    var deleteBtn = UIButton()
    
    //这个是用在toolView中的
    let timerImageView = UIImageView(frame: CGRectMake(20, 20, 40, 56))
    let tapTimerImageViewGesture = UITapGestureRecognizer()
    
    let durationImageView = UIImageView(frame: CGRectMake(20, 96, 40, 56))
    let tapDurationImageViewGesture = UITapGestureRecognizer()
    
    
    let frequencyImageView = UIImageView(frame: CGRectMake(20, 172, 40, 56))
    let tapFrequencyImageViewGesture = UITapGestureRecognizer()
    var frequencyImageViewOfTextField = UITextField()
    
    //这个是工具栏中的history视图，以及点击该视图弹出的视图内容
    let historyImageView = UIImageView(frame: CGRectMake(20, 248, 40, 56))
    let tapHistoryImageViewGesture = UITapGestureRecognizer()
    var historyImageView01TextField = UITextField()
    var historyImageView02TextField = UITextField()

    
    
    //这个是工具视图中第五个视图，以及点击该视图出现的弹出视图
    let imageView04 = UIImageView(frame: CGRectMake(20, 375, 40, 75))
    let tapImageView04Gesture = UITapGestureRecognizer()
    var timeTextField = UITextField()
    
    
    
    var contextListArray = [
        "In response to contrived or captu Motivation Operations,Teresa wilindependently request for 10 pre items,activities,or edibles.","In response to contrived or captu Motivation Operations,Teresa wilindependently request for 10 pre items,activities,or edibles."
    ]
    
    func initViewToDictionary() -> [String : UIView]{
        var dicViews : [String : UIView] = ["homeView":homeView]
        dicViews["toolView"] = toolView
        dicViews["timerImageView"] = timerImageView
        dicViews["durationImageView"] = durationImageView
        dicViews["frequencyImageView"] = frequencyImageView
        dicViews["historyImageView"] = historyImageView
        dicViews["imageView04"] = imageView04
        return dicViews
    }
    
    //这个是点击右侧工具栏中的视图，弹出的视图里面的初始化最基础的视图，和delete按钮
    func initDeleteBtnToPopupView(rect: CGRect){
        let deleteImage = UIImage(named: "Delete")
        deleteBtn.frame = rect
        deleteBtn.setBackgroundImage(deleteImage, forState: UIControlState.Normal)
        deleteBtn.addTarget(self, action: #selector(DateCecordViewController.deleteViewFromSuperview(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        popView.addSubview(popupView)
        popupView.addSubview(deleteBtn)
        popupView.backgroundColor = UIColor.init(red: 217 / 255, green: 217 / 255, blue: 217 / 255, alpha: 1)
        self.homeView.addSubview(popView)
        self.homeView.alpha = 0.8
    }
    
    
    func tapImageView04(recognizer: UITapGestureRecognizer) {
        addOneGestureRecognizer(tapImageView04Gesture, tapView: imageView04)
        popView.frame = CGRectMake(UIScreen.mainScreen().bounds.width / 2 - 200,UIScreen.mainScreen().bounds.height / 2 - 200,400,400)
        popupView.frame = CGRectMake(0,30,400,370)
        
        initDeleteBtnToPopupView(CGRectMake(370, -15, 30, 30))
        
        let timeImageView = Inits.initImageView(CGRectMake(150, 70, 95, 105), named: "Stopwatch")
        popupView.addSubview(timeImageView)
    
        let timeLabel = Inits.initLabel(CGRectMake(120,190,160,40), named: "00:23:16", fontSize: 25, textAlignment: NSTextAlignment.Center)
        timeLabel.layer.borderWidth = 0
        popupView.addSubview(timeLabel)
        
        timeTextField = Inits.initTextField(CGRectMake(100,250,200,60), named: "xxxxxxxx", fontSize: 25, textAlignment: NSTextAlignment.Center, textColor: UIColor.redColor(),delegate: self)
        timeTextField.clearsOnBeginEditing = true
        timeTextField.backgroundColor = UIColor.blackColor()
        timeTextField.clearButtonMode = .Always
        
        popupView.addSubview(timeTextField)

    }
    
    func removeAllGestureRecognizer() {
        homeView.removeGestureRecognizer(tapHomeViewGesture)
        timerImageView.removeGestureRecognizer(tapTimerImageViewGesture)
        durationImageView.removeGestureRecognizer(tapDurationImageViewGesture)
        frequencyImageView.removeGestureRecognizer(tapFrequencyImageViewGesture)
        historyImageView.removeGestureRecognizer(tapHistoryImageViewGesture)
        imageView04.removeGestureRecognizer(tapImageView04Gesture)
    }
    
    func addOneGestureRecognizer(recognizer: UITapGestureRecognizer,tapView: UIView) {
        if recognizer == tapFrequencyImageViewGesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapFrequencyImageViewGesture)
        } else if recognizer == tapHomeViewGesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapHomeViewGesture)
        } else if recognizer == tapHistoryImageViewGesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapHistoryImageViewGesture)
        } else if recognizer == tapImageView04Gesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapImageView04Gesture)
        } else if recognizer == tapTimerImageViewGesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapTimerImageViewGesture)
        } else if recognizer == tapDurationImageViewGesture {
            removeAllGestureRecognizer()
            tapView.addGestureRecognizer(tapDurationImageViewGesture)
        }
    }

    func tapTimerImageView(recognizer: UITapGestureRecognizer) {
        addOneGestureRecognizer(tapTimerImageViewGesture, tapView: timerImageView)
        popView.frame = CGRectMake(UIScreen.mainScreen().bounds.width / 2 - 250,UIScreen.mainScreen().bounds.height / 2 - 140,500,280)
        popupView.frame = CGRectMake(0,30,500,250)
        initDeleteBtnToPopupView(CGRectMake(485, -15, 30, 30))
        
        let titleImageView = UIImageView(frame: CGRectMake(90, 20, 42, 55))
        titleImageView.image = UIImage(named: "Timer")
        popupView.addSubview(titleImageView)
        
        let titleLabel = UILabel(frame: CGRectMake(150, 15, 200, 60))
        titleLabel.text = "xxxxxxxx"
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont.systemFontOfSize(40)
        popupView.addSubview(titleLabel)
        
        let timeLabel = UILabel(frame: CGRectMake(125, 90, 250, 80))
        timeLabel.text = "01:00:00"
        timeLabel.textColor = UIColor.whiteColor()
        timeLabel.textAlignment = .Center
        timeLabel.font = UIFont.systemFontOfSize(60)
        popupView.addSubview(timeLabel)
        
        let hlineView = UIView(frame: CGRectMake(0, 188, 500, 2))
        hlineView.backgroundColor = UIColor.whiteColor()
        popupView.addSubview(hlineView)
        let vlineView = UIView(frame: CGRectMake(249, 190, 2, 60))
        vlineView.backgroundColor = UIColor.whiteColor()
        popupView.addSubview(vlineView)
        
        let reqeatBtn = UIButton(frame: CGRectMake(24, 195, 200, 50))
        reqeatBtn.setTitle("Reqeat", forState: UIControlState.Normal)
        reqeatBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        popupView.addSubview(reqeatBtn)
        let okBtn = UIButton(frame: CGRectMake(275, 195, 200, 50))
        okBtn.setTitle("OK", forState: UIControlState.Normal)
        okBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        popupView.addSubview(okBtn)
        
    }
    var durationImageViewOfTextField : UITextField!
    func tapDurationImageView(recognizer: UITapGestureRecognizer) {
        addOneGestureRecognizer(tapDurationImageViewGesture, tapView: durationImageView)
        popView.frame = CGRectMake(UIScreen.mainScreen().bounds.width / 2 - 250,UIScreen.mainScreen().bounds.height / 2 - 205,500,410)
        popupView.frame = CGRectMake(0,30,500,380)
        
        initDeleteBtnToPopupView(CGRectMake(485, -15, 30, 30))
        
        let timeImageView = UIImageView(frame: CGRectMake(200,20,100,100))
        timeImageView.image = UIImage(named: "Timer2")
        popupView.addSubview(timeImageView)
        
        let hoursImageView = UIImageView(frame: CGRectMake(50,0,2,50))
        hoursImageView.image = UIImage(named: "hoursLine")
        
        timeImageView.addSubview(hoursImageView)
//        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(DateCecordViewController.didTimer), userInfo: nil, repeats: true)
//        timer.fire()
        hoursImageView.transform = CGAffineTransformIdentity
        let rotation = CGAffineTransformMakeRotation(CGFloat(M_PI_4))
        UIView.animateWithDuration(1, animations: {
            hoursImageView.layer.anchorPoint = CGPointMake(0.5,0.5)
            hoursImageView.transform = rotation
        })
    
        
        
        let timeLabel = UILabel(frame: CGRectMake(200,140,100,30))
        timeLabel.text = "00:52:16"
        timeLabel.textColor = UIColor.whiteColor()
        timeLabel.textAlignment = .Center
        timeLabel.font = UIFont.systemFontOfSize(20)
        popupView.addSubview(timeLabel)
        
        let pauseBtn = UIButton(frame: CGRectMake(140, 200, 60, 60))
        pauseBtn.layer.cornerRadius = 30
        pauseBtn.setTitle("Pause", forState: UIControlState.Normal)
        pauseBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        pauseBtn.backgroundColor = UIColor.grayColor()
        popupView.addSubview(pauseBtn)
        let cancelBtn = UIButton(frame: CGRectMake(300, 200, 60, 60))
        cancelBtn.layer.cornerRadius = 30
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        cancelBtn.backgroundColor = UIColor.grayColor()
        popupView.addSubview(cancelBtn)
        
        durationImageViewOfTextField = Inits.initTextField(CGRectMake(100,300,300,50), named: "xxxxxxxx", fontSize: 30, textAlignment: NSTextAlignment.Center, textColor: UIColor.whiteColor(), delegate: self)
        durationImageViewOfTextField.backgroundColor = UIColor.darkGrayColor()
        durationImageViewOfTextField.clearsOnBeginEditing = true
        durationImageViewOfTextField.keyboardType = .NumberPad
        durationImageViewOfTextField.clearButtonMode = .Always
        popupView.addSubview(durationImageViewOfTextField)
    }
    
    func didTimer() {
        print("timer")
    }
    
    var labelOfFrequency = UILabel()
    var labelMini = UILabel()
    func tapFrequencyImageView(recognizer: UITapGestureRecognizer) {
        addOneGestureRecognizer(tapFrequencyImageViewGesture, tapView: frequencyImageView)
        popView.frame = CGRectMake(UIScreen.mainScreen().bounds.width / 2 - 180,UIScreen.mainScreen().bounds.height / 2 - 150,360,300)
        popupView.frame = CGRectMake(0,30,360,270)
        
        initDeleteBtnToPopupView(CGRectMake(330, -15, 30, 30))
        let minusBtn = UIButton(frame: CGRectMake(50, 85, 30, 30))
        minusBtn.tag = 300
        minusBtn.setBackgroundImage(UIImage(named: "-0"), forState: UIControlState.Normal)
        
        minusBtn.addTarget(self, action: #selector(DateCecordViewController.plusOrMinusOne(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        popupView.addSubview(minusBtn)
        labelOfFrequency = Inits.initLabel(CGRectMake(130,50,100,100), named: "07", fontSize: 60, textAlignment: NSTextAlignment.Center)
        labelOfFrequency.layer.cornerRadius = 10
        labelOfFrequency.textColor = UIColor.whiteColor()
        popupView.addSubview(labelOfFrequency)
        let plusBtn = UIButton(frame: CGRectMake(280, 85, 30, 30))
        plusBtn.tag = 301
        plusBtn.setBackgroundImage(UIImage(named: "+"), forState: UIControlState.Normal)
        plusBtn.addTarget(self, action: #selector(DateCecordViewController.plusOrMinusOne(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        popupView.addSubview(plusBtn)
        labelMini = Inits.initLabel(CGRectMake(155,160,50,30), named: "07", fontSize: 15, textAlignment: NSTextAlignment.Center)
        labelMini.layer.borderWidth = 0
        labelMini.textColor = UIColor.whiteColor()
        popupView.addSubview(labelMini)
        frequencyImageViewOfTextField = Inits.initTextField(CGRectMake(30,190,300,50), named: "xxxxxxxx", fontSize: 30, textAlignment: NSTextAlignment.Center, textColor: UIColor.whiteColor(), delegate: self)
        frequencyImageViewOfTextField.backgroundColor = UIColor.darkGrayColor()
        frequencyImageViewOfTextField.clearsOnBeginEditing = true
        frequencyImageViewOfTextField.keyboardType = .NumberPad
        frequencyImageViewOfTextField.clearButtonMode = .Always
        popupView.addSubview(frequencyImageViewOfTextField)
    }
    
    //这个是当textField里面的数据变化的时候做的相应的操作
    func textFieldDidEndEditing(textField: UITextField) {
        if Int(frequencyImageViewOfTextField.text!) > 9 {
            labelMini.text = frequencyImageViewOfTextField.text
            labelOfFrequency.text = frequencyImageViewOfTextField.text
        } else {
            labelMini.text = "0" + frequencyImageViewOfTextField.text!
            labelOfFrequency.text = "0" + frequencyImageViewOfTextField.text!
        }
        
    }
    var timer : NSTimer!
    func plusOrMinusOne(btn: UIButton) {
        
        if btn.tag == 300 {
            if Int(labelOfFrequency.text!) > 10 {
                labelOfFrequency.text = "\(Int(labelOfFrequency.text!)! - 1)"
            } else if Int(labelOfFrequency.text!) < 10 || Int(labelOfFrequency.text!) == 10 {
                labelOfFrequency.text = "0" + "\(Int(labelOfFrequency.text!)! - 1)"
            }
        } else if btn.tag == 301 {
            if Int(labelOfFrequency.text!) > 9 || Int(labelOfFrequency.text!) == 9{
                labelOfFrequency.text = "\(Int(labelOfFrequency.text!)! + 1)"
            } else if Int(labelOfFrequency.text!) < 9  {
                labelOfFrequency.text = "0" + "\(Int(labelOfFrequency.text!)! + 1)"
            }
        }
        labelMini.text = labelOfFrequency.text
    }

    
    func tapHistoryImageView(recognizer: UITapGestureRecognizer) {
        addOneGestureRecognizer(tapHistoryImageViewGesture, tapView: historyImageView)
        
        popView.frame = CGRectMake(UIScreen.mainScreen().bounds.width / 2 - 300,UIScreen.mainScreen().bounds.height / 2 - 200,600,400)
        popupView.frame = CGRectMake(0,30,600,370)
        
        initDeleteBtnToPopupView(CGRectMake(570, -15, 30, 30))
        
        let historyLabel = Inits.initLabel(CGRectMake(220,20,160,40), named: "HISTORY", fontSize: 25, textAlignment: NSTextAlignment.Center)
        historyLabel.layer.borderWidth = 0
        popupView.addSubview(historyLabel)
        
        let historyImageView01 = Inits.initImageView(CGRectMake(20,80,30,30), named: "Duration icon")
        popupView.addSubview(historyImageView01)
        
        let historyImageView01Label = Inits.initLabel(CGRectMake(70,80,200,30), named: "xxxxxx", fontSize: 25, textAlignment: NSTextAlignment.Left)
        historyImageView01Label.layer.borderWidth = 0
        popupView.addSubview(historyImageView01Label)
        
        
        historyImageView01TextField = Inits.initTextField(CGRectMake(350,80,200,30), named: "xxxxxxxx", fontSize: 25, textAlignment: NSTextAlignment.Right, textColor: UIColor.redColor(),delegate: self)
        historyImageView01TextField.clearsOnBeginEditing = true
        historyImageView01TextField.clearButtonMode = .Always
        popupView.addSubview(historyImageView01TextField)
        
        let linView01 = UIView(frame: CGRectMake(0, 120, 600, 1))
        linView01.backgroundColor = UIColor.whiteColor()
        popupView.addSubview(linView01)
        
        let historyImageView02 = Inits.initImageView(CGRectMake(20,130,30,30), named: "Duration icon")
        popupView.addSubview(historyImageView02)
        
        let historyImageView02Label = Inits.initLabel(CGRectMake(70,130,200,30), named: "xxxxxx", fontSize: 25, textAlignment: NSTextAlignment.Left)
        historyImageView02Label.layer.borderWidth = 0
        popupView.addSubview(historyImageView02Label)
        
        
        historyImageView02TextField = Inits.initTextField(CGRectMake(350,130,200,30), named: "xxxxxxxx", fontSize: 25, textAlignment: NSTextAlignment.Right, textColor: UIColor.redColor(),delegate: self)
        historyImageView02TextField.clearsOnBeginEditing = true
        historyImageView02TextField.clearButtonMode = .Always
        popupView.addSubview(historyImageView02TextField)
        
        let linView02 = UIView(frame: CGRectMake(0, 170, 600, 1))
        linView02.backgroundColor = UIColor.whiteColor()
        popupView.addSubview(linView02)
    }
    
    func deleteViewFromSuperview(btn: UIButton) {
        self.homeView.alpha = 1
        for subview in popView.subviews {
            subview.removeFromSuperview()
        }
        for subview in popupView.subviews {
            subview.removeFromSuperview()
        }
        addOneGestureRecognizer(tapHomeViewGesture, tapView: homeView)
    }
    
    func swipe(recognizer:UISwipeGestureRecognizer) {
        if recognizer.direction == .Right {
            if !isLeftTableViewShow {
                isLeftTableViewShow = true
                UIView.animateWithDuration(0.1, animations: {
                    self.leftView.frame.size.width = 300
                    Inits.initLeftTableView(self.leftView, delegate: self, dataSource: self)
                    
                    let tranlate01 = CGAffineTransformMakeTranslation(300, 0)
                    self.centerView.transform = tranlate01
                    
                    let tranlate02 = CGAffineTransformMakeTranslation(300, 0)
                    self.rightView.transform = tranlate02
                })
            }
            
        } else if recognizer.direction == .Left {
            if !isToolViewShow {
                isToolViewShow = true
                UIView.animateWithDuration(0.1, animations: {
                    Inits.initToolView(self.initViewToDictionary())
                    
                    self.tapImageView04Gesture.addTarget(self, action: #selector(DateCecordViewController.tapImageView04(_:)))
                    self.imageView04.addGestureRecognizer(self.tapImageView04Gesture)
                    
                    self.tapHistoryImageViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapHistoryImageView(_:)))
                    self.historyImageView.addGestureRecognizer(self.tapHistoryImageViewGesture)
                    
                    self.tapFrequencyImageViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapFrequencyImageView(_:)))
                    self.frequencyImageView.addGestureRecognizer(self.tapFrequencyImageViewGesture)
                    
                    self.tapTimerImageViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapTimerImageView(_:)))
                    self.timerImageView.addGestureRecognizer(self.tapTimerImageViewGesture)
                    
                    self.tapDurationImageViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapDurationImageView(_:)))
                    self.durationImageView.addGestureRecognizer(self.tapDurationImageViewGesture)
                    
                    let tranlate01 = CGAffineTransformMakeTranslation(-100, 0)
                    self.rightView.transform = tranlate01
                    
                    let tranlate02 = CGAffineTransformMakeTranslation(-100, 0)
                    self.centerView.transform = tranlate02
                })
            }
            
        }
    }
    func resetAllView(recognizer:UITapGestureRecognizer) {
        if isLeftTableViewShow {
            isLeftTableViewShow = false
            UIView.animateWithDuration(0.1, animations: {
                self.centerView.transform = CGAffineTransformIdentity
                self.rightView.transform = CGAffineTransformIdentity
                self.leftView.frame.size.width -= 300
            })
        }
        if isToolViewShow {
            isToolViewShow = false
            UIView.animateWithDuration(0.1, animations: {
                self.rightView.transform = CGAffineTransformIdentity
                self.centerView.transform = CGAffineTransformIdentity
                self.toolView.removeFromSuperview()
            })
        }
    }
    
    func initDelegate() {
        notes.delegate = self
        generatlizationNotes.delegate = self
        customerTableView.delegate = self
        customerTableView.dataSource = self
    }
    
    func tapGeneratlizationOrBiView(recognizer:UITapGestureRecognizer) {
        if recognizer == tapGeneratlizationViewGesture {
            generatlizationView.hidden = false
            biView.hidden = true
            tapBiView.backgroundColor = UIColor.whiteColor()
            tapGeneratlizationView.backgroundColor = UIColor.lightGrayColor()
        } else if recognizer == tapBiViewGesture {
            generatlizationView.hidden = true
            biView.hidden = false
            tapBiView.backgroundColor = UIColor.lightGrayColor()
            tapGeneratlizationView.backgroundColor = UIColor.whiteColor()
        }
    }
    

    func initGesture() {
        swipeRight.direction = .Right
        swipeRight.addTarget(self, action: #selector(DateCecordViewController.swipe(_:)))
        homeView.addGestureRecognizer(swipeRight)
        
        swipeLeft.direction = .Left
        swipeLeft.addTarget(self, action: #selector(DateCecordViewController.swipe(_:)))
        homeView.addGestureRecognizer(swipeLeft)
        
        tapHomeViewGesture.addTarget(self, action: #selector(DateCecordViewController.resetAllView(_:)))
        homeView.addGestureRecognizer(tapHomeViewGesture)
        
        tapGeneratlizationViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapGeneratlizationOrBiView(_:)))
        tapGeneratlizationView.addGestureRecognizer(tapGeneratlizationViewGesture)
        
        tapBiViewGesture.addTarget(self, action: #selector(DateCecordViewController.tapGeneratlizationOrBiView(_:)))
        tapBiView.addGestureRecognizer(tapBiViewGesture)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDelegate()
        
        initKeyBoard()
        
        initGesture()
        
        Inits.initLabel(trialMin)
        Inits.initBtn(independentBtn)
        Inits.initBtn(fvmBtn)
        Inits.initBtn(pvpBtn)
        Inits.initBtn(minusBtn)
        Inits.initBtn(plusBtn)
        
        Inits.initBtn(generatilizationMonBtn)
        Inits.initBtn(generatilizationDadBtn)
        Inits.initBtn(generatilizationSiblingBtn)
        Inits.initBtn(generatilizationOtherBtn)
        Inits.initBtn(generatilizationIndependentBtn)
        Inits.initBtn(generatilizationFvmBtn)
        Inits.initBtn(generatilizationPvpBtn)
        Inits.initLabel(generatilizationTimeLabel)
        Inits.initBtn(generatilizationUpBtn)
        Inits.initBtn(generatilizationDownBtn)
        
    }
    
    func initKeyBoard(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DateCecordViewController.keyboardWillAppear(_:)), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DateCecordViewController.keyboardWillDisappear(_:)), name:UIKeyboardWillHideNotification, object: nil)
        
        self.view.backgroundColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1)
    }

    //当键盘升起来的时候提高位置，根据键盘升起来多少位置提高多少
    func keyboardWillAppear(notification: NSNotification) {
        
        // 获取键盘信息
        let keyboardinfo = notification.userInfo![UIKeyboardFrameBeginUserInfoKey]
        
        let keyboardheight:CGFloat = (keyboardinfo?.CGRectValue.size.height)!

        if UIScreen.mainScreen().bounds.height - self.notes.frame.origin.y < keyboardheight || UIScreen.mainScreen().bounds.height - self.generatlizationNotes.frame.origin.y < keyboardheight{
            UIView.animateWithDuration(0.2, animations: {
                self.timeTextField.superview?.frame.origin.y -= 200
                self.notes.superview?.frame.origin.y -= 220
                self.generatlizationNotes.superview?.frame.origin.y -= 200
                self.frequencyImageViewOfTextField.superview?.frame.origin.y -= 160
            })
        } else {
            UIView.animateWithDuration(0.2, animations: {
                self.view.frame.origin.y -= self.notes.frame.size.height
            })
        }
    }
    
    //在键盘收起来的时候恢复位置
    func keyboardWillDisappear(notification:NSNotification){
        UIView.animateWithDuration(0.1, animations: {
            self.timeTextField.superview?.frame.origin.y += 200
            self.notes.superview?.frame.origin.y += 220
            self.generatlizationNotes.superview?.frame.origin.y += 200
            self.frequencyImageViewOfTextField.superview?.frame.origin.y += 160
        })
    }
    
    //点击键盘上的return键隐藏键盘
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //点击屏幕其他空白地方隐藏键盘
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        notes.resignFirstResponder()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView.tag == 1000 {
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1000 {
            return contextListArray.count
        } else {
            return names.count
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.tag == 1000 {
            return 95
        } else {
            return 89
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.tag == 1000 {
            let cell = tableView.dequeueReusableCellWithIdentifier("customerLeftCell", forIndexPath: indexPath) as! CustomerLeftTableViewCell
            cell.name.text = contextListArray[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("dateCecordViewCell", forIndexPath: indexPath) as! DateCecordViewCell
            cell.imageview.image = UIImage(named: "Oval 52 + Line + Line Copy 3")
            cell.name.text = names[indexPath.row]
            cell.number.text = "0 / 5"
            return cell
        }
    }
}

