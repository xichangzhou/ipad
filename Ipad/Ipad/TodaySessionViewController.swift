//
//  TodaySessionViewController.swift
//  Ipad
//
//  Created by Li Song on 16/3/24.
//  Copyright © 2016年 Li Song. All rights reserved.
//

import UIKit

class TodaySessionViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    var contextListArray = [
        "In response to contrived or captu Motivation Operations,Teresa wilindependently request for 10 pre items,activities,or edibles.","In response to contrived or captu Motivation Operations,Teresa wilindependently request for 10 pre items,activities,or edibles."
    ]
    
    var appointments = [
        Appointment(imageName:"icon_complete.png",target:"Cheecked Out",time:"8:45AM",name:"Billy",des:"niaho",btnName:"View"),
        Appointment(imageName:"icon_progress.png",target:"Cheecked In",time:"8:45AM",name:"Billy",des:"niaho",btnName:"Check Out"),
        Appointment(imageName:"icon_upcoming.png",target:"Ready in 3 hrs",time:"8:45AM",name:"Billy",des:"niaho",btnName:"Check In")]
    
    @IBOutlet weak var activityTableView: UITableView!
    @IBOutlet weak var leftTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leftTableView.estimatedRowHeight = 120
        leftTableView.rowHeight = UITableViewAutomaticDimension
        
        initNavigationBar()
    }
    
    func initNavigationBar() {
        //自定义navigationBar
        let navigationBar : UINavigationBar = UINavigationBar(frame: CGRectMake(0,0,UIScreen.mainScreen().bounds.width,90))
        let item = UINavigationItem()
        
        navigationBar.barTintColor = UIColor.blackColor()
        
        let treatmentImageView = UIImageView(frame: CGRectMake(5,10,24,24))
        let treatmentImage = UIImage(named: "file-text-o - FontAwesome")
        treatmentImageView.image = treatmentImage
        
        let treatmentBtn = UIButton(frame: CGRectMake(30,10,150,24))
        treatmentBtn.setTitle("Treatment Plan", forState: UIControlState.Normal)
        treatmentBtn.addTarget(self, action: #selector(TodaySessionViewController.tappedTreatment(_:)), forControlEvents:
            UIControlEvents.TouchUpInside)
        
        let behaviorImageView = UIImageView(frame: CGRectMake(180,10,24,24))
        let behaviorImage = UIImage(named: "file-text-o - FontAwesome")
        behaviorImageView.image = behaviorImage
        
        let behaviorBtn = UIButton(frame: CGRectMake(200,10,150,24))
        behaviorBtn.setTitle("Behavior Plan", forState: UIControlState.Normal)
        
        let summaryBtn = UIButton(frame: CGRectMake(360,10,160,24))
        summaryBtn.setTitle("SUMMARY", forState: UIControlState.Normal)
        summaryBtn.layer.cornerRadius = 5
        summaryBtn.layer.borderWidth = 1
        summaryBtn.layer.borderColor = UIColor.whiteColor().CGColor

        
        let rightView = UIView(frame: CGRectMake(0,0,540,44))

        rightView.addSubview(treatmentImageView)
        rightView.addSubview(treatmentBtn)
        rightView.addSubview(behaviorImageView)
        rightView.addSubview(behaviorBtn)
        rightView.addSubview(summaryBtn)
        
        let btnoneItem = UIBarButtonItem(customView: rightView)
        item.rightBarButtonItem = btnoneItem
        
        
        let leftBackBtn = UIButton(frame: CGRectMake(0,0,60,44))
        leftBackBtn.setTitle("< Back", forState: UIControlState.Normal)
        leftBackBtn.addTarget(self, action: #selector(TodaySessionViewController.backFrontView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        item.leftBarButtonItem = UIBarButtonItem(customView: leftBackBtn)
        
        navigationBar.pushNavigationItem(item, animated: true)
        self.view.addSubview(navigationBar)
    }
    
    func backFrontView(btn : UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tappedTreatment(btn : UIButton) {
        let treatmentTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TreatmentTableViewController")
        self.navigationController?.pushViewController(treatmentTableViewController!, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.tag == 10 {
            return 120
        } else if tableView.tag == 11 {
            return 44
        }
        return 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 10 {
            return contextListArray.count
        } else if tableView.tag == 11 {
            return appointments.count
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView.tag == 10 {
            let cell = tableView.dequeueReusableCellWithIdentifier("lefttableviewcell", forIndexPath: indexPath) as! TodaySessionViewCell
            
            cell.des.text = contextListArray[indexPath.row]
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("activitiescell", forIndexPath: indexPath) as! ActivitiesTableViewCell
        cell.imageview.image = UIImage(named: appointments[indexPath.row].imageName)
        cell.des.text = appointments[indexPath.row].des
        cell.time.text = appointments[indexPath.row].time
        return cell
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
