    //
    //  AppointmentTableViewController.swift
    //  Ipad
    //
    //  Created by Li Song on 16/3/24.
    //  Copyright © 2016年 Li Song. All rights reserved.
    //
    
    import UIKit
    import JTCalendar
    
    class AppointmentTableViewController: UIViewController, JTCalendarDelegate ,UITableViewDelegate,UITableViewDataSource{
      @IBOutlet weak var horizontalCalendarView: JTHorizontalCalendarView!
      var calendarManager: JTCalendarManager!
      var calendarSelectDate = NSDate()
      
      var appointments = [
        Appointment(imageName:"icon_complete.png",target:"Cheecked Out",time:"8:45AM",name:"Billy",des:"niaho",btnName:"View"),
        Appointment(imageName:"icon_progress.png",target:"Cheecked In",time:"8:45AM",name:"Billy",des:"niaho",btnName:"Check Out"),
        Appointment(imageName:"icon_upcoming.png",target:"Ready in 3 hrs",time:"8:45AM",name:"Billy",des:"niaho",btnName:"Check In")]
      
      
      func initCalendar() {
        self.calendarManager = JTCalendarManager()
        self.calendarManager.contentView = self.horizontalCalendarView
        self.calendarManager.settings.weekModeEnabled = true
        self.calendarManager.delegate = self
        self.calendarManager.setDate(NSDate())
      }
      
      
      override func viewDidLoad() {
        super.viewDidLoad()
        initCalendar()
        
        
        let rect : CGRect = (self.navigationController?.navigationBar.frame)!
        self.navigationController?.navigationBar.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, 104)
      }
      
      override func viewWillAppear(animated: Bool) {
        self.title = "\(calendarSelectDate)"
      }
      
      
      // MARK: - Table view data source
      
      func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
      }
      
      func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return appointments.count
      }
      
      func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
      }
      
      
      func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("appointmentcell", forIndexPath: indexPath) as! AppointmentTableViewCell
        
        let appointment = appointments[indexPath.row]
        
        cell.imageView?.image = UIImage(named: appointment.imageName)
        cell.targetLabel.text = appointment.target
        cell.timeLabel.text = appointment.time
        cell.nameLabel.text = appointment.name
        cell.desLabel.text = appointment.des
        cell.tappedBtn.setTitle(appointment.btnName, forState: .Normal)
        
        return cell
      }
      
      func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
      }
    
      
      func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
          let checkInViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CheckInViewController")
          
          navigationController?.pushViewController(checkInViewController!, animated: true)
        }
      }
      
      
      /*
       // Override to support conditional editing of the table view.
       override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
       // Return false if you do not want the specified item to be editable.
       return true
       }
       */
      
      /*
       // Override to support editing the table view.
       override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
       if editingStyle == .Delete {
       // Delete the row from the data source
       tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
       } else if editingStyle == .Insert {
       // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
       }
       }
       */
      
      /*
       // Override to support rearranging the table view.
       override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
       
       }
       */
      
      /*
       // Override to support conditional rearranging of the table view.
       override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
       // Return false if you do not want the item to be re-orderable.
       return true
       }
       */
      
      /*
       // MARK: - Navigation
       
       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       // Get the new view controller using segue.destinationViewController.
       // Pass the selected object to the new view controller.
       }
       */
      
      
      func calendar(calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        let calendarDayView: JTCalendarDayView = dayView as! JTCalendarDayView
        calendarDayView.hidden = false
        
        calendarDayView.backgroundColor = .whiteColor()
        calendarDayView.textLabel.font = UIFont.systemFontOfSize(25)
        if self.calendarManager.dateHelper.date(NSDate(), isTheSameDayThan: calendarDayView.date) && NSDate().isEqualToDate(self.calendarSelectDate){
          calendarDayView.circleView.hidden = false
          calendarDayView.circleView.backgroundColor = UIColor(red: 249/255, green: 132/255, blue: 121/255, alpha: 1)
          calendarDayView.dotView.backgroundColor = .whiteColor()
          calendarDayView.textLabel.textColor = .whiteColor()
        } else if self.calendarManager.dateHelper.date(self.calendarSelectDate, isTheSameDayThan: calendarDayView.date){
          calendarDayView.circleView.hidden = false
          calendarDayView.circleView.backgroundColor = UIColor(red: 249/255, green: 132/255, blue: 121/255, alpha: 1)
          calendarDayView.dotView.backgroundColor = .whiteColor()
          calendarDayView.textLabel.textColor = .whiteColor()
        } else {
          calendarDayView.circleView.hidden = true
          calendarDayView.textLabel.textColor = .blackColor()
        }
      }
      
      func calendar(calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        let calendarDayView: JTCalendarDayView = dayView as! JTCalendarDayView
        // Use to indicate the selected date
        self.calendarSelectDate = calendarDayView.date
        //  titleLabel.text = dateFormatter.stringFromDate(appointmentSelectedDate)
        // Animation for the circleView
        calendarDayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
        UIView.transitionWithView(calendarDayView, duration: 0.1, options: UIViewAnimationOptions.LayoutSubviews, animations: { () -> Void in
          calendarDayView.circleView.transform = CGAffineTransformIdentity
          calendar.reload()
          }, completion: nil)
        
        // Load the previous or next page if touch a day from another month
        if self.calendarManager.dateHelper.date(self.horizontalCalendarView.date, isTheSameMonthThan: calendarDayView.date) {
          if self.horizontalCalendarView.date.compare(calendarDayView.date) == NSComparisonResult.OrderedAscending {
            //        self.calendarContentView.loadNextPageWithAnimation()
          } else {
            //        self.calendarContentView.loadPreviousPageWithAnimation()
          }
        }
      }
    }
