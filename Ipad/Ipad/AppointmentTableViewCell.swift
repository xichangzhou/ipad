//
//  AppointmentTableViewCell.swift
//  Ipad
//
//  Created by Li Song on 16/3/24.
//  Copyright © 2016年 Li Song. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {

  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var targetLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var desLabel: UILabel!
  @IBOutlet weak var tappedBtn: UIButton!
  

  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
