//
//  ActivitiesTableViewCell.swift
//  Ipad
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 Li Song. All rights reserved.
//

import UIKit

class ActivitiesTableViewCell: UITableViewCell {
   
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var des: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
